package com.mybigday.photo;

import android.graphics.Bitmap;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mybigday.photo.api.PhotoAPI;
import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.impl.ext.LruDiskCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.IoUtils;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Eric on 2015/11/2.
 */
public class Application extends android.app.Application {

    public final static String MYBIGDAY_BASE_URL = "http://pepper.mybigday.com.tw/api";

    private PhotoAPI restClient;
    private AmazonS3Client s3;
    private TransferUtility transferUtility;
    private Picasso picasso;

    @Override
    public void onCreate() {
        super.onCreate();

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        OkHttpClient okHttpClient = new OkHttpClient();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new AndroidLog("RETROFIT"))
                .setEndpoint(MYBIGDAY_BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .setConverter(new GsonConverter(gson))
//                .setErrorHandler(new RetrofitErrorHandler())

                .build();
        restClient = restAdapter.create(PhotoAPI.class);

        setupS3();
        setupPicasso();
        setupImageLoader();

    }

    public PhotoAPI getRestClient() {
        return restClient;
    }

    public AmazonS3Client getS3() {
        return s3;
    }

    public TransferUtility getTransferUtility() {
        return transferUtility;
    }

    public Picasso getPicasso() {
        return picasso;
    }

    private void setupS3() {
        // Create an S3 client
        AWSCredentials credentialsProvider = new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return "AKIAJN2VA4FVEHSSHGYA";
            }

            @Override
            public String getAWSSecretKey() {
                return "gGSluBShsee/Ka0CPtnywec6fMVt6fbinxuAon/C";
            }
        };
        s3 = new AmazonS3Client(credentialsProvider);
        s3.setRegion(Region.getRegion(Regions.DEFAULT_REGION));

        transferUtility = new TransferUtility(s3, this);
    }

    private void setupPicasso() {
        OkHttpClient picassoClient = new OkHttpClient();

//        picassoClient.interceptors().add(chain -> {
//            Request newRequest = chain.request().newBuilder()
//                    .addHeader("Authorization", "Basic bXliaWdkYXk6bUxpZmU0VGVhbQ==")
//                    .build();
//            return chain.proceed(newRequest);
//        });

        File httpCacheDir = new File(getExternalCacheDir(), "mybigday-cache");
        long httpCacheSize = 100 * 1024 * 1024; // 10 MiB
        try {
            Cache cache = new Cache(httpCacheDir, httpCacheSize);
            picassoClient.setCache(cache);
        } catch (Exception e) {
            e.printStackTrace();
        }

        picasso = new Picasso.Builder(this).downloader(new OkHttpDownloader(picassoClient)).build();
    }

    private void setupImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.logo_navy)
                .build();
        ImageLoader.getInstance().init(new ImageLoaderConfiguration.Builder(this)
//                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
//                .memoryCacheSize(2 * 1024 * 1024)
//                .diskCacheSize(100 * 1024 * 1024)
//                .diskCacheFileCount(100)
                .defaultDisplayImageOptions(defaultOptions)
                .build());
    }
}
