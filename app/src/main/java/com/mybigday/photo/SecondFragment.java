package com.mybigday.photo;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybigday.photo.question.QuestionActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class SecondFragment extends Fragment {

    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_second, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick(R.id.button_start) void startClicked() {
        startActivity(new Intent(getActivity(), QuestionActivity.class));
    }

    //TODO: http://city.udn.com/54733/3009661
    //A：肯定的言詞 B：精心的時刻 C：接受禮物 D：服務的行動 E：身體的接觸
    //Set1, Set2的那個左下角說明拿掉, description 拿掉
    // 肯定言詞: %s分
    // 肯定言詞: %s分
    // 肯定言詞: %s分
    // 肯定言詞: %s分
    // 肯定言詞: %s分
}
