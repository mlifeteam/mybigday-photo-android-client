package com.mybigday.photo.album;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mybigday.photo.Application;
import com.mybigday.photo.PrintActivity;
import com.mybigday.photo.R;
import com.mybigday.photo.api.gson.Photo;
import com.mybigday.photo.api.gson.Result;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AlbumActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    @Bind(R.id.top_panel)
    LinearLayout topPanel;

    @Bind(R.id.gridview)
    GridView gridView;

    @Bind(R.id.bottom_panel)
    LinearLayout bottomPanel;

    @Bind(R.id.bottom_panel2)
    FrameLayout bottomPanel2;

    @Bind(R.id.selfie) ImageView selfie;

    private AlbumPhotoAdapter adapter;
    private List<Photo> photos = new ArrayList<>();
    private List<Photo> pickedPhotos = new ArrayList<>();
    private List<ImageView> faces = new ArrayList<>();
    private final int PICK_MAX = 3;
    private final int SNACKBAR_DEFAULT_HEIGHT = 210;

    private Application app;
    private ProgressDialog progressDialog;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        ButterKnife.bind(this);

        app = (Application) getApplication();
        realm = Realm.getInstance(this);
        fetchByFace(null);

        adapter = new AlbumPhotoAdapter(this, photos);
        gridView.setAdapter(adapter);
        gridView.setOnItemLongClickListener(this);
        gridView.setOnItemClickListener(this);

        String selfiePath = getSharedPreferences("signup", MODE_PRIVATE).getString("selfie", "");
        Log.d("test", selfiePath);
//        Picasso.with(this).load(new File(selfiePath)).fit().centerCrop().into(selfie);
        ImageLoader.getInstance().displayImage("file://" + selfiePath, selfie);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.button_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

        bottomPanel.setVisibility(View.VISIBLE);
        ImageView chosenImage = (ImageView) getLayoutInflater().inflate(R.layout.item_album_chosen, bottomPanel, false);
        chosenImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomPanel.removeView(view);
            }
        });
        app.getPicasso()
                .load(photos.get(i).getPhotoUrl())
                .placeholder(R.drawable.logo_white)
                .fit().centerCrop()
                .into(chosenImage);

        if (bottomPanel.getChildCount() < PICK_MAX && !pickedPhotos.contains(photos.get(i))) {
            pickedPhotos.add(photos.get(i));
            bottomPanel.addView(chosenImage);
            Snackbar snackbar = Snackbar.make(view, String.format("還剩%d 張", PICK_MAX - bottomPanel.getChildCount()), Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorNavyTransparent));
            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) snackbar.getView().getLayoutParams();
            lp.height = bottomPanel.getMeasuredHeight();
            if (lp.height == 0) lp.height = SNACKBAR_DEFAULT_HEIGHT;
            snackbar.getView().setLayoutParams(lp);
            TextView t = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
            t.setGravity(Gravity.CENTER);
            snackbar.show();
        }
        if (bottomPanel.getChildCount() == PICK_MAX) {
            bottomPanel2.setVisibility(View.VISIBLE);
        }

        return true;
    }

    @OnClick(R.id.selfie) void filterSelf() {
        cleanFaces();
        fetchByFace(null);
    }

    @OnClick(R.id.filter_all)
    void fileterAll() {
        cleanFaces();
        // TODO: load all
        fetchWithTag(null);
    }

    @OnClick(R.id.filter_face) void loadFace() {
        if (faces.size() > 0) {
            cleanFaces();
        } else {
            app.getRestClient().fetchFace(new Callback<Result>() {
                @Override
                public void success(Result result, Response response) {
                    cleanFaces();
                    Gson gson = new Gson();
                    for (Object obj : result.objects) {

                        Photo photo = gson.fromJson(gson.toJsonTree(obj).getAsJsonObject(), Photo.class);

                        ImageView face = (ImageView) getLayoutInflater().inflate(R.layout.item_album_face, topPanel, false);
                        face.setTag(photo);
                        face.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Photo photo = (Photo) view.getTag();
                                fetchByFace(photo.id);
                            }
                        });
//                        app.getPicasso().load(photo.getPhotoUrl()).into(face);
                        ImageLoader.getInstance().displayImage(photo.getPhotoUrl(), face);
                        topPanel.addView(face);
                        faces.add(face);
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }

    }

    @OnClick(R.id.button_reset) void resetClicked() {
        bottomPanel2.setVisibility(View.GONE);
        bottomPanel.removeAllViews();
        bottomPanel.setVisibility(View.GONE);
    }

    @OnClick(R.id.button_print) void printClicked() {

        // Generate UUID
        String uuid = UUID.randomUUID().toString().replace("-", "");
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((999999 - 100000) + 1) + 100000;
        uuid = String.format("%06d", randomNum);
        getSharedPreferences("album", MODE_PRIVATE).edit().putString("UUID", uuid).apply();

        String name = getSharedPreferences("signup", MODE_PRIVATE).getString("name", "");
        for (Photo photo: pickedPhotos) {
            photo.addTagList = new String[] {"print_" + uuid, String.format("guest_%s", name.replace("[\\s]", ""))};
            app.getRestClient().updatePhoto(photo.id, photo, new Callback<Result>() {
                @Override
                public void success(Result result, Response response) {
                    Toast.makeText(AlbumActivity.this, "Picture picked", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }

        startActivity(new Intent(this, PrintActivity.class));
        finish();
    }

    private void cleanFaces() {
        for (ImageView face : faces) {
            topPanel.removeView(face);
        }
        faces.clear();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        List<String> urls = new ArrayList<>();
        for (Photo p: photos) {
            urls.add(p.getPhotoUrl());
        }

        Intent intent = new Intent(this, PhotoActivity.class);
        intent.putExtra("index", i);
        intent.putExtra("photos", urls.toArray(new String[urls.size()]));
        startActivity(intent);
    }

    private void fetchWithTag(String tags) {
        showProgressDialog("Loading...");
        app.getRestClient().fetchPhoto(tags, new Callback<Result>() {
            @Override
            public void success(Result result, Response response) {
                photos.clear();
                dismissProgressDialog();
                Gson gson = new Gson();
                for (Object obj : result.objects) {
                    Photo photo = gson.fromJson(gson.toJsonTree(obj), Photo.class);

                    if (photo.storage.finish) {
                        photos.add(photo);
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgressDialog();
                Toast.makeText(AlbumActivity.this, "Timeout", Toast.LENGTH_SHORT).show();
                Log.e("RETROFIT", "TIMEOUT:" + error.getLocalizedMessage(), error);
            }
        });

    }

    private void fetchByFace(String photoId) {
        showProgressDialog("Loading...");
        if (photoId == null) photoId = getSharedPreferences("signup", MODE_PRIVATE).getString("photoId", "");
        app.getRestClient().fetchPhotoByFace(photoId, new Callback<Result>() {
            @Override
            public void success(Result result, Response response) {
                photos.clear();
                dismissProgressDialog();
                Gson gson = new Gson();
                for (Object obj : result.objects) {
                    Photo photo = gson.fromJson(gson.toJsonTree(obj), Photo.class);

                    if (photo.storage.finish) {
                        photos.add(photo);
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgressDialog();
                Toast.makeText(AlbumActivity.this, "Timeout", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showProgressDialog(String message) {
        progressDialog = ProgressDialog.show(this, "", message);
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static class AlbumPhotoAdapter extends BaseAdapter {
        private Context context;
        private List<Photo> photos;

        public AlbumPhotoAdapter(Context context, List<Photo> photos) {
            this.context = context;
            this.photos = photos;
        }

        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public Object getItem(int i) {
            return photos.get(i);
        }

        @Override
        public long getItemId(int id) {
            return id;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);
            ImageView thumb = (ImageView) convertView;
            if (convertView == null) {
                thumb = (ImageView) inflater.inflate(R.layout.item_album_thumb, parent, false);
            }
            String photo = photos.get(position).getPhotoUrl();

//            Application app = (Application)((AlbumActivity) context).getApplication();
//            app.getPicasso().load(photo).placeholder(R.drawable.logo_navy).fit().centerCrop().into(thumb);
            ImageLoader.getInstance().displayImage(photo, thumb);

            return thumb;
        }
    }

}
