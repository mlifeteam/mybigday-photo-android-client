package com.mybigday.photo.album;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.mybigday.photo.FirstFragment;
import com.mybigday.photo.R;
import com.mybigday.photo.SecondFragment;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoViewAttacher;

public class PhotoActivity extends AppCompatActivity {

    @Bind(R.id.viewpager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ButterKnife.bind(this);

        viewPager.setAdapter(new SectionsPagerAdapter(getFragmentManager()));
        viewPager.setCurrentItem(getIntent().getIntExtra("index", 0));
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PhotoFragment.newInstance(position, getIntent().getStringArrayExtra("photos"));
        }

        @Override
        public int getCount() {
            return getIntent().getStringArrayExtra("photos").length;
        }
    }
}
