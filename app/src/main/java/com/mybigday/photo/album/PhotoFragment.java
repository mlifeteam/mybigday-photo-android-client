package com.mybigday.photo.album;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybigday.photo.Application;
import com.mybigday.photo.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoView;

public class PhotoFragment extends Fragment {

    private int index;
    private String[] photos;

    @Bind(R.id.image)
    PhotoView imageView;

    public PhotoFragment() {
        // Required empty public constructor
    }

    public static PhotoFragment newInstance(int index, String[] photos) {
        PhotoFragment fragment = new PhotoFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        args.putStringArray("photos", photos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            index = getArguments().getInt("index");
            photos = getArguments().getStringArray("photos");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_photo, container, false);
        ButterKnife.bind(this, rootView);

//        Application app = (Application) getActivity().getApplication();
//        app.getPicasso().load(photos[index]).fit().centerInside().into(imageView);
        ImageLoader.getInstance().displayImage(photos[index], imageView);

        return rootView;
    }

}
