package com.mybigday.photo.api;

import com.mybigday.photo.api.gson.Result;
import com.mybigday.photo.api.gson.Photo;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Eric on 2015/11/1.
 */
public interface PhotoAPI {
    @GET("/photo")
    void fetchPhoto(@Query(value = "tag_list", encodeValue = false) String tagList, Callback<Result> cb);

    @POST("/photo")
    void uploadPhoto(@Body Photo photo, Callback<Result> cb);

    @PUT("/photo/{photo_id}")
    void updatePhoto(@Path("photo_id") String photoId, @Body Photo photo, Callback<Result> callback);

    @GET("/photo/{photo_id}/upload_finish")
    void uploadPhotoFinish(@Path("photo_id") String photoId, Callback<Result> cb);

    @GET("/photo/{photo_id}/face")
    void fetchPhotoByFace(@Path("photo_id") String photoId, Callback<Result> cb);

    @GET("/face")
    void fetchFace(Callback<Result> cb);

    @GET("/photo/md5_check")
    void md5Check();

}
