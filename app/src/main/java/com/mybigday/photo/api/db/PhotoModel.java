package com.mybigday.photo.api.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Eric on 2015/11/2.
 */
public class PhotoModel extends RealmObject {

    @PrimaryKey
    private String id;
    private String url;

    public PhotoModel() {

    }

    public PhotoModel(String id, String url) {
        this.id = id;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
