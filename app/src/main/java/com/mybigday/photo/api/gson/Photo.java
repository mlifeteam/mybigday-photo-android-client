package com.mybigday.photo.api.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Eric on 2015/11/2.
 */
public class Photo {
    @SerializedName("_id")
    public String id;
    @Expose public String title;
    @Expose public String description;
    @Expose public String[] tagList;
    @Expose public String[] addTagList;
    @Expose public String md5;

    public String getPhotoUrl() {
        return String.format("http://pepper.mybigday.com.tw/api/photo/%s/file", id);
    }

    public Storage storage;

    //            "expired_datetime": "2015-11-01T17:18:40.457Z",
//                    "__v": 0,
//                    "last_modify_datetime": "2015-11-01T16:48:40.583Z",
//                    "create_datetime": "2015-11-01T16:48:39.887Z",
//                    "md5": "xxx",
//                    "_id": "56364267239e04a2013c26ab",
//                    "tag_list": [],
//            "storage": {
//                "type": "aws",
//                        "bucket": "mybigday-photo-storage",
//                        "key": "prod/56364267239e04a2013c26ab"
//            },

    public Photo(String title, String description, String[] tagList, String md5) {
        this.title = title;
        this.description = description;
        this.tagList = tagList;
        this.md5 = md5;
    }
}
