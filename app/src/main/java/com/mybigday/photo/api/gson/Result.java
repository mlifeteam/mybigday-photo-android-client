package com.mybigday.photo.api.gson;

import com.google.gson.annotations.Expose;

/**
 * Created by Eric on 2015/11/2.
 */
public class Result<T> {
    @Expose public Boolean success;
    @Expose public T[] objects;
}
