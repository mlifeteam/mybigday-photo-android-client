package com.mybigday.photo.api.gson;

/**
 * Created by Eric on 2015/11/2.
 */
public class Storage {
    public String type;
    public String bucket;
    public String key;
    public Boolean finish;
}


//            "expired_datetime": "2015-11-01T17:18:40.457Z",
//                    "__v": 0,
//                    "last_modify_datetime": "2015-11-01T16:48:40.583Z",
//                    "create_datetime": "2015-11-01T16:48:39.887Z",
//                    "md5": "xxx",
//                    "_id": "56364267239e04a2013c26ab",
//                    "tag_list": [],
//            "storage": {
//                "type": "aws",
//                        "bucket": "mybigday-photo-storage",
//                        "key": "prod/56364267239e04a2013c26ab"
//            },
//            "description": "test photo",
//                    "title": "test photo"