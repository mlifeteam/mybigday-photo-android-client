package com.mybigday.photo.api.gson;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Eric on 2015/11/2.
 */
public class UploadInfo {
    @SerializedName("Policy")
    public String policy;
    @SerializedName("Signature")
    public String signature;
    public String key;
    @SerializedName("AWSAccessKeyId")
    public String awsAccessKeyId;
    public String bucket;
}

//            "uploadInfo": {
//            "Policy": "eyJleHBpcmF0aW9uIjoiMjAxNS0xMS0wMVQxNzoxODo0MC40NTdaIiwiY29uZGl0aW9ucyI6W1siZXEiLCIkYnVja2V0IiwibXliaWdkYXktcGhvdG8tc3RvcmFnZSJdLFsic3RhcnRzLXdpdGgiLCIka2V5IiwicHJvZC81NjM2NDI2NzIzOWUwNGEyMDEzYzI2YWIiXSxbInN0YXJ0cy13aXRoIiwiJENvbnRlbnQtVHlwZSIsImltYWdlL2pwZWciXSxbImNvbnRlbnQtbGVuZ3RoLXJhbmdlIiwxMDI0MCwxMDQ4NTc2MF1dfQ==",
//                    "Signature": "nxlEsWU9zqZI3uBQIbPEEAasQpo=",
//                    "key": "prod/56364267239e04a2013c26ab",
//                    "Content-Type": "image/jpeg",
//                    "AWSAccessKeyId": "AKIAJN2VA4FVEHSSHGYA",
//                    "bucket": "mybigday-photo-storage",
//                    "expiredDatetime": "2015-11-01T17:18:40.457Z"
