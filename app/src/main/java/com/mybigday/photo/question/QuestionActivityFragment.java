package com.mybigday.photo.question;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mybigday.photo.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class QuestionActivityFragment extends Fragment {

    private String[] optionsA = new String[]{
            "我喜歡收到寫滿讚美與肯定的小紙條",
            "我喜歡和在我心目中佔有特殊地位的人獨處",
            "我喜歡收到禮物",
            "有人幫我做事，我就會覺得被愛",
            "當我所愛、所景仰的人攬著我的肩膀，我就會有被愛的感覺",
            "我喜歡和朋友或所愛的人到處走走",
            "愛的具體象徵（禮物）對我很重要",
            "我喜歡和我所喜歡的人促膝長談",
            "我喜歡和好友及所愛的人在一起",
            "我喜歡聽到別人接納我的話",
            "我喜歡和朋友與所愛的人一起做同一件事",
            "別人的表現要比他的言語更能感動我",
            "我珍惜別人的讚美，儘量避免受到批評",
            "當我和人聊天或一起做事時，我會覺得與他很親近",
            "我喜歡聽到別人稱讚我的成就",
            "我喜歡朋友或所愛的人走過我身邊時，故意用身體觸碰我的感覺",
            "當朋友或所愛的人幫助我完成工作，我會覺得被愛",
            "我喜歡聽到別人稱讚我的外表",
            "在我心目中有特殊地位的人觸碰我的身體時，我覺得有安全感",
            "我很感激在我心目中有特殊地位的人為我付出那麼多",
            "我很喜歡被人呵護備至的感覺",
            "有人送我生日禮物時，我會覺得被愛及受重視",
            "有人送我禮物，我就知道他有想到我的需要",
            "我很感激有人耐心聽我說話而且不插嘴",
            "我喜歡知道我所愛的人因為關心我，幫我做家事或買麵包等",
            "我喜歡和最親近的人牽手、擁抱、親吻",
            "我喜歡聽到有人向我表示感謝",
            "朋友或所愛的人所送的禮物，我會特別珍惜",
            "有人熱心做我所要求的事時，我會覺得被愛",
            "我每天都需要身體的接觸"
    };
    private String[] optionsB = new String[]{
            "我喜歡被擁抱的感覺",
            "每當有人給我實際的幫助，我就會覺得他是愛我的",
            "我有空就喜歡去探訪朋友和所愛的人",
            "有人碰觸我的身體，我就會覺得被愛",
            "當我所愛、所景仰的人送我禮物，我就會有被愛的感覺",
            "我喜歡和在我心目中有特殊地位的人擊掌或牽手",
            "受到別人的肯定讓我有被愛的感覺",
            "我喜歡聽到別人說我很漂亮，很迷人或很有氣質",
            "我喜歡收到朋友或所愛的人贈送的禮物",
            "如果有人幫我的忙，我會知道他是愛我的",
            "我喜歡聽到別人對我說友善的話",
            "被擁抱讓我覺得與對方很親近，也覺得自己很重要",
            "送我許多小禮物要比送我一份大禮物更能感動我",
            "朋友或所愛的人若常常與我有身體的接觸，我會覺得與他很親近",
            "當別人勉強自己為我做一件事，我會覺得他很愛我",
            "我喜歡別人聽我說話，而且表現出興趣十足的樣子",
            "我很喜歡收到朋友或所愛的人送的禮物",
            "當別人願意體諒我的感受時，我會有被愛的感覺",
            "服務的行動讓我覺得被愛",
            "我喜歡收到在我心目中有特殊地位的人送我禮物",
            "我很喜歡被人服務的感覺",
            "有人在我生日那天對我說出特別的話，我會覺得被愛",
            "有人幫我作家事，我會覺得被愛",
            "我很感激有人記得某個特別日子並且送我禮物",
            "我喜歡和在我心目中有特殊地位的人一起去逛街、旅行",
            "有人不為了特別理由而送我禮物，我會覺得很開心",
            "與人交談時，我喜歡對方注視我的眼睛",
            "朋友或所愛的人碰觸我的身體，那種感覺真好",
            "聽到別人對我表示感激，我會覺得被愛",
            "我每天都需要肯定的言語（如：別人表達感激我的付出和努力）"
    };
    private String[] valuesA = new String[]{
            "A", "B", "C", "D", "E",
            "B", "C", "E", "B", "A",
            "B", "D", "A", "B", "A",
            "E", "D", "A", "E", "D",
            "B", "C", "C", "B", "D",
            "E", "A", "C", "D", "E"
    };
    private String[] valuesB = new String[] {
            "E", "D", "B", "E", "C",
            "E", "A", "A", "C", "D",
            "A", "E", "C", "E", "D",
            "B", "C", "B", "D", "C",
            "D", "A", "D", "C", "B",
            "C", "B", "E", "A", "A"
    };
    private int currentIndex = -1;
    private int a,b,c,d,e;

    @Bind(R.id.option1)
    TextView option1;

    @Bind(R.id.option2)
    TextView option2;

    public QuestionActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question, container, false);
        ButterKnife.bind(this, rootView);
        option1.setText(optionsA[0]);
        option2.setText(optionsB[0]);
        currentIndex = 0;
        a = b = c = d = e = 0;
        return rootView;
    }

    @OnClick(R.id.option1) void option1() {
        next();
        String value = valuesA[currentIndex];
        switch (value) {
            case "A": a++; break;
            case "B": b++; break;
            case "C": c++; break;
            case "D": d++; break;
            case "E": e++; break;
        }
    }

    @OnClick(R.id.option2) void option2() {
        next();
        String value = valuesB[currentIndex];
        switch (value) {
            case "A": a++; break;
            case "B": b++; break;
            case "C": c++; break;
            case "D": d++; break;
            case "E": e++; break;
        }
    }

    private void next() {
        if (optionsA.length -1 > currentIndex) {
            currentIndex++;
            option1.setText(optionsA[currentIndex]);
            option2.setText(optionsB[currentIndex]);
        } else {
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment, ResultFragment.newInstance(a, b, c, d, e))
                    .commit();
        }
    }
}
