package com.mybigday.photo.question;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.mybigday.photo.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResultFragment extends Fragment {

    private int a, b, c, d, e;

    @Bind(R.id.radar_chart)
    RadarChart radarChart;

    @Bind(R.id.score_a)
    TextView scoreA;
    @Bind(R.id.score_b)
    TextView scoreB;
    @Bind(R.id.score_c)
    TextView scoreC;
    @Bind(R.id.score_d)
    TextView scoreD;
    @Bind(R.id.score_e)
    TextView scoreE;

    private String[] mParties = new String[] {
            "A", "B", "C", "D", "E"
    };

    // A：肯定的言詞 B：精心的時刻 C：接受禮物 D：服務的行動 E：身體的接觸

    public ResultFragment() {
        // Required empty public constructor
    }

    public static ResultFragment newInstance(int a, int b, int c, int d, int e) {
        ResultFragment fragment = new ResultFragment();
        Bundle args = new Bundle();
        args.putInt("a", a);
        args.putInt("b", b);
        args.putInt("c", c);
        args.putInt("d", d);
        args.putInt("e", e);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            a = getArguments().getInt("a");
            b = getArguments().getInt("b");
            c = getArguments().getInt("c");
            d = getArguments().getInt("d");
            e = getArguments().getInt("e");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_result, container, false);
        ButterKnife.bind(this, rootView);
        setData();

        scoreA.setText(String.format("%02d分", a));
        scoreB.setText(String.format("%02d分", b));
        scoreC.setText(String.format("%02d分", c));
        scoreD.setText(String.format("%02d分", d));
        scoreE.setText(String.format("%02d分", e));

        return rootView;
    }

    public void setData() {

        int cnt = 5;

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
//        for (int i = 0; i < cnt; i++) {
//            yVals1.add(new Entry((float) (Math.random() * mult) + mult / 2, i));
//        }
        yVals1.add(new Entry((float)a, 0));
        yVals1.add(new Entry((float)b, 1));
        yVals1.add(new Entry((float)c, 2));
        yVals1.add(new Entry((float)d, 3));
        yVals1.add(new Entry((float)e, 4));

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < cnt; i++)
            xVals.add(mParties[i % mParties.length]);

        RadarDataSet set1 = new RadarDataSet(yVals1, "Set 1");
        set1.setColor(Color.GRAY);
        set1.setDrawFilled(true);
        set1.setLineWidth(2f);

        ArrayList<RadarDataSet> sets = new ArrayList<RadarDataSet>();
        sets.add(set1);

        RadarData data = new RadarData(xVals, sets);
//        data.setValueTypeface(tf);
        data.setValueTextSize(20f);
        data.setDrawValues(false);

        radarChart.setData(data);
        radarChart.setDescriptionColor(Color.TRANSPARENT);
        radarChart.getLegend().setEnabled(false);
        radarChart.setWebColor(Color.LTGRAY);
        radarChart.setWebColorInner(Color.LTGRAY);
        radarChart.setTouchEnabled(false);

        radarChart.invalidate();
    }

    @OnClick(R.id.button_done) void finish() {
        getActivity().finish();
    }
}
