package com.mybigday.photo.signup;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.google.gson.Gson;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mybigday.photo.Application;
import com.mybigday.photo.R;
import com.mybigday.photo.api.gson.Photo;
import com.mybigday.photo.api.gson.Result;
import com.mybigday.photo.api.gson.UploadInfo;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelfieFragment extends Fragment implements ImageChooserListener {

    private ImageChooserManager imageChooserManager;
    private String imageChooserFilepath;
    private int imageChooserType;
    private AlertDialog imageChooserDialog;

    private Application app;
    private TransferUtility transferUtility;
    private SharedPreferences pref;
    private Context context;

    @Bind(R.id.button_selfie)
    ImageView selfieButton;
    @Bind(R.id.chosen_image)
    ImageView chosenImageView;

    public SelfieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (Application) getActivity().getApplication();
        transferUtility = app.getTransferUtility();
        pref = getActivity().getSharedPreferences("signup", Context.MODE_PRIVATE);
        context = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_selfie, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }


    @OnClick({R.id.button_selfie, R.id.chosen_image})
    void selfieClicked() {
        imageChooserDialog = pickPicture();
    }

    private AlertDialog pickPicture() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.widget_pick_picture, null);
        View pickPicture = dialoglayout.findViewById(R.id.pick_a_picture);
        View takePicture = dialoglayout.findViewById(R.id.take_a_picture);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialoglayout);
        pickPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickPicture(ChooserType.REQUEST_PICK_PICTURE);
            }
        });
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickPicture(ChooserType.REQUEST_CAPTURE_PICTURE);
            }
        });
        return builder.show();
    }

    void pickPicture(int chooserType) {
        if (chooserType != ChooserType.REQUEST_PICK_PICTURE &&
                chooserType != ChooserType.REQUEST_CAPTURE_PICTURE) {
            throw new Error("can only pick or capture picture");
        }
        imageChooserType = chooserType;
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        imageChooserManager.setImageChooserListener(this);
        try {
            imageChooserFilepath = imageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this,
                imageChooserType, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(imageChooserFilepath);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE
                || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        }
    }

    @Override
    public void onImageChosen(ChosenImage chosenImage) {
        imageChooserFilepath = chosenImage.getFileThumbnail();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                selfieButton.setVisibility(View.GONE);
                chosenImageView.setVisibility(View.VISIBLE);
                Picasso.with(getActivity()).load(new File(imageChooserFilepath)).into(chosenImageView);
                if (imageChooserDialog != null && imageChooserDialog.isShowing())
                    imageChooserDialog.dismiss();
            }
        });

    }

    @Override
    public void onError(String s) {

    }

    @OnClick(R.id.button_done)
    void doneClicked() {

        if (imageChooserFilepath != null) {
            // TODO: save image to server
            upload();
            // 一進來02 -> 03 這個沒問題然後就直接新增一張照片到後端，並且加上兩個tag, 一個是guest_#加上剛剛選的賓客名稱#，一個是selfie
            SharedPreferences pref = getActivity().getSharedPreferences("signup", Context.MODE_PRIVATE);
            pref.edit().putString("selfie", imageChooserFilepath).apply();
            // On finish
            getActivity().finish();
        } else {
            Toast.makeText(getActivity(), "@@", Toast.LENGTH_SHORT).show();
        }

    }

    private void upload() {
        String name = getActivity().getSharedPreferences("signup", Context.MODE_PRIVATE).getString("name", "");


        Photo p = new Photo(
                name,
                "",
                new String[]{"selfie", String.format("guest_%s", name.replaceAll("[\\s,\\d]", ""))},
                "");

        app.getRestClient().uploadPhoto(p, new Callback<Result>() {
            @Override
            public void success(Result result, Response response) {
                Gson gson = new Gson();
                final Photo photo = gson.fromJson(gson.toJsonTree(result.objects[0]).getAsJsonObject().getAsJsonObject("photo"), Photo.class);
                UploadInfo info = gson.fromJson(gson.toJsonTree(result.objects[0]).getAsJsonObject().getAsJsonObject("uploadInfo"), UploadInfo.class);


                pref.edit().putString("photoId", photo.id).apply();

                TransferObserver transferObserver = transferUtility.upload(info.bucket, info.key, new File(imageChooserFilepath));
                transferObserver.setTransferListener(new TransferListener() {

                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        // do something
                        Toast.makeText(context, state.toString(), Toast.LENGTH_SHORT).show();
                        if (state.equals(TransferState.COMPLETED)) {
                            app.getRestClient().uploadPhotoFinish(photo.id, new Callback<Result>() {
                                @Override
                                public void success(Result result, Response response) {

                                }

                                @Override
                                public void failure(RetrofitError error) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        int percentage = (int) (bytesCurrent / bytesTotal * 100);
                        //Display percentage transferred to user
                        Toast.makeText(context, String.format("%d%%", percentage), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        // do something
                        Toast.makeText(context, ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }

                });
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

}
