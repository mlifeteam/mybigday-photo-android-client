package com.mybigday.photo.signup;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mybigday.photo.R;

public class SignupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getFragmentManager().beginTransaction()
                .add(R.id.fragment, new SignupFragment())
                .addToBackStack(null)
                .commit();
    }
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0 ){
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
