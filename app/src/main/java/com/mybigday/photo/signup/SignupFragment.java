package com.mybigday.photo.signup;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mybigday.photo.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class SignupFragment extends Fragment {

    private BaseAdapter adapter;
    private int selectedItem = -1;
    @Bind(R.id.listview)
    ListView listView;

    public SignupFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);

        ButterKnife.bind(this, rootView);
        adapter = new BaseAdapter() {

            String[] array = new String[]{
                    "李 承達",
                    "莊 孟婕",
                    "趙 邦媛",
                    "葉 偉琳",
                    "邱 盈瑄",
                    "杜 昱晨",
                    "許 可珊",
                    "彭 雅琪",
                    "林 介蘭",
                    "黃 士蔚",
                    "邱 騰輝",
                    "侯 德湘",
                    "劉 宗穎",
                    "陳 安君",
                    "潘 韋嘉",
                    "陳 建宇",
                    "王 三福",
                    "李 奕賢",
                    "章 坤",
                    "呂 佳慧",
                    "杜 彬湧",
                    "林 儷如",
                    "陳 宛妮",
                    "耿 宛真",
                    "王 裕勛",
                    "王 湘雅",
                    "賴 宜姍",
                    "周 鈺璇",
                    "莊 小茹",
                    "羅 立誠",
                    "姚 佑謙",
                    "鍾 勝偉",
                    "楊 凱勝",
                    "華通 200 總裁",
                    "華通 500 副總裁",
                    "華通 800 副總裁",
                    "楊 志通",
                    "林 建志",
                    "邱 博煜",
                    "李 詠廉",
                    "陳 正諭",
                    "曾 朝鴻",
                    "吳 俊良",
                    "郭 韋廷",
                    "潘 建宏",
                    "黃 怡傑",
                    "彭 元隆",
                    "葉 則亮",
                    "邱 垂銨",
                    "蕭 志偉",
                    "江 士標",
                    "林 峻宇",
                    "蔡 瑋軒",
                    "歐 怡廷",
                    "劉 滿英",
                    "劉 立業",
                    "高 金益",
                    "吳 英龍",
                    "陳 克平",
                    "周 崇仁",
                    "許 庭耀",
                    "賈 哥哥",
                    "葉 哥哥",
                    "陳 哥哥",
                    "李 昊",
                    "陳 宥任",
                    "張 惠普",
                    "嚴 忠和",
                    "施 閎凱",
                    "吳 劉藝",
                    "嚴 珠容",
                    "賴 德錦",
                    "吳 團義",
                    "劉 卓群",
                    "唐 貴陵",
                    "羅 金琳",
                    "徐 興華",
                    "潘 可柯",
                    "吳 月春",
                    "鄭 在勇",
                    "張 光杰",
                    "潘 裕如",
                    "虞 錦清",
                    "周 世賢",
                    "陳 家駿",
                    "陳 守訓",
                    "陳 文穎",
                    "倪 雲平",
                    "黃 明輝",
                    "黃 明瑞",
                    "黃 明榮",
                    "黃 秀蘭",
                    "黃 秀雲",
                    "阿公阿嬤",
                    "蘇 茂榮",
                    "蘇 茂南",
                    "蘇 敏玉",
                    "何 玉仙",
                    "陳 孝侖",
                    "葉 書詠",
                    "陳 冠中",
                    "廖 奕桓",
                    "杜 衍輝",
                    "蔡 錫錚",
                    "江 介良",
                    "戚 禎庭",
                    "賴 暄",
                    "姚 錦程",
                    "黃 履峰",
                    "黃 建成",
                    "黃 昭蓉",
                    "蘇 登堂",
                    "蘇 茂杞",
                    "四叔",
                    "板橋阿舅",
                    "新竹阿舅",
                    "四姑",
                    "楊 林左",
                    "周 天錦",
                    "李 國緯",
                    "施 坰宗",
                    "Praveen & 鎂齡",
                    "高 永清",
                    "廖 文烺",
                    "吳 素真",
                    "蘇 昭莉",
                    "楊 滿美",
                    "汪 玉霞",
                    "鄭 素嬌",
                    "何 澤龍",
                    "潘 素華",
                    "卓 優近",
                    "黃 國芳",
                    "黃 國和",
                    "劉 麗婷",
                    "許 正霖",
                    "黃 靜寧",
                    "蘇 上凱",
                    "嚴 延年",
                    "蘇 登相",
                    "子約 吳總",
                    "黃 明山",
                    "林 廣哲",
                    "石 泰山",
                    "Carlos",
                    "何 澤龍",
                    "洪 振杰",
                    "羅 維霓",
                    "王 慧蘭",
                    "楊 靜雯",
                    "阿蘭（明輝）",
                    "山粟企業有限公司",
            };
            @Override
            public int getCount() {
                return array.length;
            }

            @Override
            public Object getItem(int i) {
                return array[i];
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                Log.d("test", "i:" + listView.getSelectedItem());
                if (view == null) {
                    view = getActivity().getLayoutInflater().inflate(android.R.layout.simple_list_item_1, null);
                }
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setText(array[i]);
                view.setBackgroundColor(selectedItem != i ?
                        getResources().getColor(android.R.color.transparent) :
                        getResources().getColor(android.R.color.darker_gray));
                return view;
            }


        };
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedItem = i;
                adapter.notifyDataSetChanged();
            }
        });
        return rootView;
    }

    @OnClick(R.id.button_done) void doneClicked() {
        if (selectedItem != -1) {
            Toast.makeText(getActivity(), "XD", Toast.LENGTH_LONG).show();
            // Save username
            SharedPreferences pref = getActivity().getSharedPreferences("signup", Context.MODE_PRIVATE);
            pref.edit().putString("name", (String)adapter.getItem(selectedItem)).apply();


            // Go to selfie fragment
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment, new SelfieFragment())
                    .addToBackStack(null)
                    .commit();
        } else {
            Toast.makeText(getActivity(), "@@", Toast.LENGTH_LONG).show();
        }
    }
}
